# sys-config

This repository contains scripts to automate Linux system setup for myself.  Installs my preferred security, development, and GNOME themes.

## How to Use

To use the utility, clone the repository and run `./configure.sh`.  The script will then prompt you to install various utilities.

## Contributing

If you're using this utility and want extra functionality, feel free to make a pull request or leave an issue.  I'll add any requested enhancements as quickly as I can, but pull requests will always be quicker.  The code here is provided entirely as-is.