export SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

install_spotify() {
  sudo dnf install snapd
  sudo ln -s /var/lib/snapd/snap /snap
  snap install spotify
}

install_steam() {
  if [[ $OS == "ubuntu" ]]; then
    sudo add-apt-repository multiverse
    sudo apt update && sudo apt install steam
  elif [[ $OS == "fedora" ]]; then
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak -y install flathub com.valvesoftware.Steam
  fi
}

install_standardnotes() {
  wget -nc https://github.com/standardnotes/desktop/releases/download/v3.0.24/Standard-Notes-3.0.24.AppImage
  mkdir -p ~/Apps/StandardNotes
  mv ./Standard-Notes-3.0.24.AppImage ~/Apps/StandardNotes/Standard-Notes.AppImage
  chmod a+x ~/Apps/StandardNotes/Standard-Notes.AppImage
  AppImageLauncher ~/Apps/StandardNotes/Standard-Notes.AppImage
}

install_bitwarden() {
  wget -nc -O BitWarden.AppImage https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=appimage
  mkdir ~/Apps
  mv BitWarden.AppImage ~/Apps/BitWarden.AppImage
  chmod a+x ~/Apps/BitWarden.AppImage
  AppImageLauncher ~/Apps/BitWarden.AppImage
}

install_mac_theme() {
  sudo dnf install sassc glib2-devel gtk-murrine-engine gtk2-engines
  git clone https://github.com/vinceliuice/Mojave-gtk-theme.git ~/Documents/git/mac_mojave
  ~/Documents/git/mac_mojave/install.sh
}

install_arc_theme() {
  if [[ $OS == "ubuntu" ]]; then
    sudo apt install arc-theme
  elif [[ $OS == "fedora" ]]; then
    sudo dnf install arc-theme
  fi
}

# Detect distro and set variable
# Checking different options in order of likelihood
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$(echo "$NAME" | tr '[:upper:]' '[:lower:]')
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    echo "not implemented"
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    echo "not implemented"
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

if [[ $OS == "ubuntu" ]]; then
  sudo apt install terminator qbittorrent vlc calibre gimp wine winetricks chrome-gnome-shell hugo

  wget -nc https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.1.2/appimagelauncher_2.1.2-travis961.e405182.bionic_amd64.deb
  sudo apt install ./appimagelauncher_*.deb
elif [[ $OS == "fedora" ]]; then
  echo "Installing useful utilities"
  sudo dnf install terminator qbittorrent vlc calibre gimp wine winetricks manuskript gramps hugo

  wget -nc https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.1.0/appimagelauncher-2.1.0-travis897.d1be7e7.x86_64.rpm
  sudo dnf install appimagelauncher-*.rpm
fi

read -p "Install Spotify? (Y/N)" spotify
case $spotify in
  [Yy]* ) install_spotify $OS;;
  [Nn]* ) echo "Not installing Spotify.";;
esac

read -p "Install Steam? (Y/N)" steam
case $steam in
  [Yy]* ) install_steam $OS;;
  [Nn]* ) echo "Not installing Steam.";;
esac

read -p "Install Standard Notes? (Y/N)" snotes
case $snotes in
  [Yy]* ) install_standardnotes;;
  [Nn]* ) echo "Not installing Standard Notes.";;
esac

read -p "Install BitWarden? (Y/N)" bitwar
case $bitwar in
  [Yy]* ) install_bitwarden;;
  [Nn]* ) echo "Not installing Standard Notes.";;
esac

read -p "Install Mac theme? (Y/N)" theme
case $theme in
  [Yy]* ) install_mac_theme;;
  [Nn]* ) echo "Not installing Mac theme.";;
esac

read -p "Install Arc theme? (Y/N)" theme
case $theme in
  [Yy]* ) install_arc_theme;;
  [Nn]* ) echo "Not installing Arc theme.";;
esac

gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'
