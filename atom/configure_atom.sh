export SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo $SCRIPTDIR

if [[ $1 == "ubuntu" ]]; then
  if [[ $(dpkg -l | grep " atom ") ]]; then
    echo "Atom is already installed."
  else
    echo "Installing Atom."
    sudo apt install curl
    curl https://atom-installer.github.com/v1.44.0/atom-amd64.deb > ./atom.deb
    sudo apt install ./atom.deb
    rm ./atom.deb
  fi
fi

if [[ $1 == "fedora" ]]; then
  if [[ $(rpm -qa | grep ^atom-) ]]; then
    echo "Atom already installed."
  else
    echo "Installing Atom."
    curl https://atom-installer.github.com/v1.44.0/atom.x86_64.rpm > ./atom.rpm
    sudo dnf install ./atom.rpm
    rm ./atom.rpm
    sudo dnf install pandoc aspell
  fi
fi

$SCRIPTDIR/plugins/install_plugins.sh

cp ./configs/config.cson ~/.atom/config.cson
cp ./configs/toolbar.cson ~/.atom/toolbar.cson
